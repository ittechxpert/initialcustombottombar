import * as React from 'react';
import Icons from 'react-native-vector-icons/Ionicons';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {Utils} from '../utils/Utils';

const Icon = props => {
  const iconName = () => {
    if (Utils.isIos()) {
      return 'ios-' + props.name;
    }
    return 'md-' + props.name;
  };
  return (
    <Icons
      style={props?.style}
      name={props?.noPrefix ? props.name : iconName()}
      color={props?.color}
      size={props?.size}
      onPress={props?.onPress}
    />
  );
};
Icon.defaultProps = {
  color: 'white',
  size: hp('20'),
  noPrefix: false,
};

export default Icon;
