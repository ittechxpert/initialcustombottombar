import {View, Text, Image} from 'react-native';
import React from 'react';

const Profile = () => {
  return (
    <View style={{flex: 1}}>
      {/* <Text style={{fontSize: 20, color: 'red'}}>Splash</Text> */}
      <Image source={require('../assets/brown.jpg')} />
    </View>
  );
};

export default Profile;
