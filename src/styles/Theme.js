export default {
  $WHITE_COLOR: '#FFFFFF',
  $TEXTCOLOR: '#7F7F7F',
  $BLACKCOLOR: '#000000',
  $GREYCOLOR: '#989898',
  $GREEN: '#8FB023',
  $YELLOW: '#BF9B07',
};

export const FONT_WEIGHT_REGULAR = '400';
export const FONT_WEIGHT_BOLD = '700';
// Top Background - #263238
// Buttons & all - #FF4E00
// Background other then white - #F8F8F8
// Heading - #282F39
// Text - #7F7F7F
