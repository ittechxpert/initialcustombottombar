import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Theme from './Theme';

export const Typography = {
  title: {
    fontSize: wp('7%'),
    fontWeight: '600',
  },
  heading: {
    fontSize: wp('4.2%'),
    color: Theme.$DARK_COLOR,
    fontFamily: 'Montserrat-Bold',
  },
  subHeading: {
    fontSize: wp('4.2%'),
    color: Theme.$DARK_COLOR,
    fontWeight: '400',
    lineHeight: 21.5,
    letterSpacing: 0.1,
    fontFamily: 'Montserrat-Regular',
  },
  medium: {
    color: Theme.$VERMILION,
    fontSize: wp('4%'),
    fontWeight: '600',
    fontFamily: 'Montserrat-SemiBold',
  },
  paragraph: {
    color: Theme.$DARK_COLOR,
    lineHeight: 21.5,
    fontFamily: 'Montserrat-SemiBold',
  },
  lightText: {
    fontSize: wp('3.6%'),
    color: '#000',
    fontFamily: 'Montserrat-Regular',
  },
  large: {
    color: Theme.$DARK_COLOR,
  },
  xlarge: {
    color: Theme.$DARK_COLOR,
  },
  errorText: {
    fontSize: wp('3.7%'),
  },
};

export const MarginSpacing = {
  finally: {
    marginLeft: wp('0.5%'),
    marginRight: wp('0.5%'),
    marginTop: hp('0.5%'),
    marginBottom: hp('0.5%'),
  },
  tiny: {
    marginLeft: wp('1%'),
    marginRight: wp('1%'),
    marginTop: hp('1%'),
    marginBottom: hp('1%'),
  },
  small: {
    marginLeft: wp('2%'),
    marginRight: wp('2%'),
    marginTop: hp('2%'),
    marginBottom: hp('2%'),
  },
  regular: {
    marginLeft: wp('3%'),
    marginRight: wp('3%'),
    marginTop: hp('3%'),
    marginBottom: hp('3%'),
  },
  large: {
    marginLeft: wp('4%'),
    marginRight: wp('4%'),
    marginTop: hp('4%'),
    marginBottom: hp('4%'),
  },
  extraLarge: {
    marginLeft: wp('5%'),
    marginRight: wp('5%'),
    marginTop: hp('5%'),
    marginBottom: hp('5%'),
  },
};

export const PaddingSpacing = {
  tiny: {
    padding: wp('2.25%'),
    paddingRight: wp('0.25%'),
    paddingTop: hp('0.25%'),
    paddingBottom: hp('0.25%'),
  },
  small: {
    padding: wp('0.5%'),
    paddingRight: wp('0.5%'),
    paddingTop: hp('0.5%'),
    paddingBottom: hp('0.5%'),
  },
  regular: {
    padding: wp('1%'),
    paddingRight: wp('1%'),
    paddingTop: hp('1%'),
    paddingBottom: hp('1%'),
  },
  large: {
    padding: wp('1.5%'),
    paddingRight: wp('1.5%'),
    paddingTop: hp('1.5%'),
    paddingBottom: hp('1.5%'),
  },
  extraLarge: {
    padding: wp('10%'),
    paddingRight: wp('10%'),
    paddingTop: hp('6%'),
    paddingBottom: hp('5%'),
  },
};
export const textAlign = {
  align: {
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
};
