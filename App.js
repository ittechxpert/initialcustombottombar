import React, {useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import Profile from './src/components/Profile';
import Search from './src/components/Search';
import Home from './src/components/Home';
import Email from './src/components/Email';
import Setting from './src/components/Setting';
import InitialCustomBottomBar from './src/screens/CustomBottombar';

const App = () => {
  return (
    <NavigationContainer>
      <InitialCustomBottomBar
        useBottomText={true}
        ProfileText={'Profile'}
        SearchText={'Search'}
        HomeText={'Home'}
        EmailText={'Email'}
        SettingText={'Setting'}
        tab1Image={require('./src/assets/user.png')}
        tab2Image={require('./src/assets/search-interface-symbol.png')}
        tab3Image={require('./src/assets/home.png')}
        tab4Image={require('./src/assets/email.png')}
        tab5Image={require('./src/assets/setting.png')}
        tab1Component={<Profile />}
        tab2Component={<Search />}
        tab3Component={<Home />}
        tab4Component={<Email />}
        tab5Component={<Setting />}
        bottomBarItemStyle={''}
        bottomBarImageStyle={{}}
        BottomTextStyle={''}
        bottomBarStyle={''}
        mk
        activeImageStyle={''}
      />
    </NavigationContainer>
  );
};

export default App;
